package com.devco.agencia.viajes.user_interface;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.targets.Target;

public class RegistroPasajeroConfirmacionCorreo {

	public static Target NOMBRE_TITULAR = Target.the("el nombre {0}").located(By.id("nombreTitular"));

	public static Target TIPO_DOCUMENTO = Target.the("el tipo documento {0}")
			.located(By.xpath("//*[@id=\"inputGroupSelect01\"]"));

	public static Target NUMERO_DOCUMENTO = Target.the("el numero documento {0}")
			.located(By.xpath("/html/body/app-root/app-registro-pasajero/div/form/div[3]/input"));

	public static Target NUMERO_TARJETA = Target.the("el numero tarjeta {0}")
			.located(By.xpath("/html/body/app-root/app-registro-pasajero/div/form/div[4]/input"));

	public static Target FRANQUICIA = Target.the("el franquicia{0}")
			.located(By.xpath("/html/body/app-root/app-registro-pasajero/div/form/div[5]/input"));

	public static Target FECHA = Target.the("el fecha {0}").located(By.xpath("/html/body/app-root/app-registro-pasajero/div/form/div[6]/input"));

	public static Target CODIGO = Target.the("el codigo {0}").located(By.xpath("/html/body/app-root/app-registro-pasajero/div/form/div[7]/input"));

	public static Target EMAIL = Target.the("el email {0}").located(By.xpath("/html/body/app-root/app-registro-pasajero/div/form/div[8]/input"));

	public static Target BOTON_NUEVO_PASAJERO = Target.the("").located(By.xpath("//*[@id=\"btnPasajero\"]"));

	public static Target BOTON_SUBMIT = Target.the("")
			.located(By.id("GuardadoBoton"));
	
	public static Target TITULO_DE_LA_PAGINA = Target.the("el titulo de la pagina en h2 {0}")
            .located(By.xpath("/html/body/app-root/app-reserva-viaje-form/div/h1"));
}
