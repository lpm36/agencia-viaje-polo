package com.devco.agencia.viajes.tasks;

import static net.serenitybdd.screenplay.Tasks.instrumented;

import net.serenitybdd.screenplay.Performable;

public class PasajeroFaltaCorreoConfirmacionBuilder {

	private String nombreTitular;
	private String tipoDocumento;
	private String numeroDocumento;
	private String numeroTarjetaCredito;
	private String franquicia;
	private String fecha;
	private String codigoVerificacion;
	private String email;

	public PasajeroFaltaCorreoConfirmacionBuilder nombre(String nombre) {
		this.nombreTitular = nombre;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder tipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder numeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder numeroTarjetaCredito(String numeroTarjetaCredito) {
		this.numeroTarjetaCredito = numeroTarjetaCredito;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder franquicia(String franquicia) {
		this.franquicia = franquicia;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder fecha(String fecha) {
		this.fecha = fecha;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder codigoVerificacion(String codigoVerificacion) {
		this.codigoVerificacion = codigoVerificacion;
		return this;
	}

	public PasajeroFaltaCorreoConfirmacionBuilder email(String email) {
		this.email = email;
		return this;
	}

	

	public Performable build() {
		return instrumented(PasajeroFaltaCorreoConfirmacion.class, nombreTitular, tipoDocumento, numeroDocumento,
				numeroTarjetaCredito, franquicia, fecha, codigoVerificacion, email);
	}
}
