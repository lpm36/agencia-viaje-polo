package com.devco.agencia.viajes.tasks;

import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.BOTON_NUEVO_PASAJERO;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.BOTON_SUBMIT;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.CODIGO;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.EMAIL;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.FECHA;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.FRANQUICIA;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.NOMBRE_TITULAR;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.NUMERO_DOCUMENTO;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.NUMERO_TARJETA;
import static com.devco.agencia.viajes.user_interface.RegistroPasajeroConfirmacionCorreo.TIPO_DOCUMENTO;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class PasajeroFaltaCorreoConfirmacion implements Task {

	private String nombreTitular;
	private String tipoDocumento;
	private String numeroDocumento;
	private String numeroTarjetaCredito;
	private String franquicia;
	private String fecha;
	private String codigoVerificacion;
	private String email;

	public PasajeroFaltaCorreoConfirmacion(String nombreTitular, String tipoDocumento, String numeroDocumento,
			String numeroTarjetaCredito, String franquicia, String fecha, String codigoVerificacion, String email) {
		this.nombreTitular = nombreTitular;
		this.tipoDocumento = tipoDocumento;
		this.numeroDocumento = numeroDocumento;
		this.numeroTarjetaCredito = numeroTarjetaCredito;
		this.franquicia = franquicia;
		this.fecha = fecha;
		this.codigoVerificacion = codigoVerificacion;
		this.email = email;
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(BOTON_NUEVO_PASAJERO),
				WaitUntil.angularRequestsHaveFinished(),
				Enter.theValue(nombreTitular).into(NOMBRE_TITULAR),
				SelectFromOptions.byVisibleText(tipoDocumento).from(TIPO_DOCUMENTO),
				Enter.theValue(numeroDocumento).into(NUMERO_DOCUMENTO),
				Enter.theValue(numeroTarjetaCredito).into(NUMERO_TARJETA),
				WaitUntil.angularRequestsHaveFinished(),
				Enter.theValue(franquicia).into(FRANQUICIA),
				Enter.theValue(fecha).into(FECHA), Enter.theValue(codigoVerificacion).into(CODIGO),
				Enter.theValue(email).into(EMAIL), Click.on(BOTON_SUBMIT));
	}

	public static PasajeroFaltaCorreoConfirmacionBuilder paraPasajero() {
		return new PasajeroFaltaCorreoConfirmacionBuilder();
	}
}
