package com.devco.agencia.viajes.step_definitions;

import static com.devco.agencia.viajes.user_interface.Opcion.AgenciaViajes;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

import com.devco.agencia.viajes.questions.ElTituloDeLaPaginaNuevoPasajero;
import com.devco.agencia.viajes.tasks.BuscarVuelosDisponibles;
import com.devco.agencia.viajes.tasks.Navegar;
import com.devco.agencia.viajes.tasks.PasajeroFaltaCorreoConfirmacion;
import com.devco.agencia.viajes.tasks.SeleccionarVuelos;

import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class ValidarCorreoStepDefinitions {

	@Before
	public void set_the_stage() {
		OnStage.setTheStage(new OnlineCast());
	}

	@Dado("^que(.*) ha decidido ingresar un nuevo pasajero")
	public void haDecididoIngresarNuevoPasajero(String nombrePersona) throws Throwable {
		theActorCalled(nombrePersona)
				.attemptsTo(
						Navegar.a(AgenciaViajes)
						);
	}
/*
	@Cuando("^no registra el correo de confirmacion")
	public void nomRegistraCorreoElectronicoConfirmacion(String nombre, String tipoDocumento, String numeroDocumento,
			String numeroTarjetaCredito, String franquicia, String fecha, String codigoVerificacion, String email)
			throws Throwable {
		theActorInTheSpotlight().attemptsTo(PasajeroFaltaCorreoConfirmacion.paraPasajero().nombre(nombre)
				.tipoDocumento(tipoDocumento).numeroDocumento(numeroDocumento)
				.numeroTarjetaCredito(numeroTarjetaCredito).franquicia(franquicia).fecha(fecha)
				.codigoVerificacion(codigoVerificacion).email(email).build());
	}*/
	
	@Cuando("^no registra el correo de confirmacion")
	public void nomRegistraCorreoElectronicoConfirmacion()
			throws Throwable {
		theActorInTheSpotlight().attemptsTo(PasajeroFaltaCorreoConfirmacion.paraPasajero().nombre("Luis")
				.tipoDocumento("Pasaporte").numeroDocumento("58469874")
				.numeroTarjetaCredito("2364892485").franquicia("VISA").fecha("28-06-2019")
				.codigoVerificacion("08669").email("luis@sura.pe").build());
	}

	@Entonces("^reciber un mensaje de error$")
	public void mostrarDatosIngresados() throws Throwable {
		theActorInTheSpotlight().should(seeThat(ElTituloDeLaPaginaNuevoPasajero.es(), not(isEmptyString())));
	}
}
