import { Component, OnInit } from '@angular/core';
import {ReservaViaje} from '../reserva-viaje';
import {ViajesApiService} from '../viajes-api.service';
import {Vuelo} from '../vuelo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reserva-viaje-form',
  templateUrl: './reserva-viaje-form.component.html',
  styleUrls: ['./reserva-viaje-form.component.css']
})
export class ReservaViajeFormComponent implements OnInit {
  consultaViajesDisponiblesRealizada:boolean = false;
  vueloIdaTitle:String = "Vuelo Ida"
  vueloRegresoTitle:String = "Vuelo Regreso"
  clases = ['economica', 'ejecutiva', 'primera_clase'];
  vuelo = new Vuelo('','','','','',false)
  reserva = new ReservaViaje('', 'medellin-colombia','lima-peru','2019-01-01','2019-02-01', 1,
  'economica',this.vuelo, this.vuelo);
  vuelosDisponibles:Array<Vuelo> = [];

  submitted = (localStorage.getItem('key')!=null && localStorage.getItem('key')!=undefined?localStorage.getItem('key'):false);

  onSubmit() { this.submitted = true; }

  newReservaViaje() {

  }

  buscarVuelosDisponibles (){
    this.viajesApi.getVuelosDisponibles(this.reserva).subscribe((data: {}) => {
      console.log(data);
      this.vuelosDisponibles = data as Array<Vuelo>;
    });
    this.consultaViajesDisponiblesRealizada = true;
  }

  disableSubmit(formIsValido: boolean){
    return (!formIsValido && this.isEmpty(this.reserva.vueloIda) == null && this.isEmpty(this.reserva.vueloRegreso == null))
  }

  isEmpty(objeto) {
    if (objeto == null) return true;
    if (objeto.isArray(objeto) || objeto.isString(objeto)) return objeto.length === 0;
    for (var key in objeto) if (objeto.has(objeto, key)) return false;
    return true;
  };

  constructor(public viajesApi:ViajesApiService,
    private router: Router
    ) { }

  ngOnInit() {
    this.validar();

  }

  validar(){
    this.submitted = (localStorage.getItem('key')!=null && localStorage.getItem('key')!=undefined?localStorage.getItem('key'):false);
    localStorage.removeItem('key');
  }

  nuevoPasajero(){
    this.router.navigate(['pasajero']);
  }

  guardarReserva (){
    this.submitted=false
    console.log("Va a llamar :D");
    this.viajesApi.guardarReserva(this.reserva).subscribe((data: {}) => {
      console.log("Llamo xD");
      console.log('data: ',data);
      //this.vuelosDisponibles = data as Array<Vuelo>;
    });
    this.consultaViajesDisponiblesRealizada = true;
  }

  //this.router.navigate(['listar-retenciones']);
  //localStorage.setItem(key, 'Value');
  //let myItem = localStorage.getItem(key);
}
