import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservaViajeFormComponent } from './reserva-viaje-form/reserva-viaje-form.component';
import { RegistroPasajeroComponent } from './registro-pasajero/registro-pasajero.component';

const routes: Routes = [
  { path: 'viaje', component: ReservaViajeFormComponent },
  { path: 'pasajero', component: RegistroPasajeroComponent },
  { path: '', redirectTo: 'viaje', pathMatch: 'full' },
  { path: '**', component: ReservaViajeFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
