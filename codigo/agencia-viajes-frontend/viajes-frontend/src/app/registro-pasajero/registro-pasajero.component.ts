import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro-pasajero',
  templateUrl: './registro-pasajero.component.html',
  styleUrls: ['./registro-pasajero.component.css']
})
export class RegistroPasajeroComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  guardar(){
    this.router.navigate(['viaje']);
  localStorage.setItem('key', 'true');
  //let myItem = localStorage.getItem(key);
  }
}
