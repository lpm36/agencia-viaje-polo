package com.devco.viajes.dominio.servicios;

import org.springframework.stereotype.Service;

import com.devco.viajes.dominio.entidades.ReservaViaje;

@Service
public interface ReservaService {

	public ReservaViaje guardarReserva(ReservaViaje reserva);
}
