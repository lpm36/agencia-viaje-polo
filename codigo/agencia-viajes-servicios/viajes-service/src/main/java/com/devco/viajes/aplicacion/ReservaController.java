package com.devco.viajes.aplicacion;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devco.viajes.dominio.entidades.ReservaViaje;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class ReservaController {

	@PostMapping(value = "/reserva")
	public ResponseEntity<ReservaViaje> reserva(@RequestBody ReservaViaje reservaViaje) {

		return ResponseEntity.status(HttpStatus.CREATED).body(reservaViaje);
	}
}
