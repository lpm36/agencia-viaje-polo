package com.devco.viajes.dominio.entidades;

import java.security.NoSuchAlgorithmException;

import com.devco.viajes.dominio.entidades.ReservaViaje.ReservaViajeBuilder;
import com.devco.viajes.dominio.valueobjects.ClaseViaje;
import com.devco.viajes.excepciones.ParametrosIncorrectos;

public class Pasajero {
	String id;
	String nombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String tipoDocumento;
	String nroDocumento;
	String nroTarjetaCredito;
	String franquiciaTarjetaCredito;
	String mesVencimiento;
	String anioVencimiento;
	String cvc;
	String email;
	
	public Pasajero() {
    }
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNroDocumento() {
		return nroDocumento;
	}
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getNroTarjetaCredito() {
		return nroTarjetaCredito;
	}
	public void setNroTarjetaCredito(String nroTarjetaCredito) {
		this.nroTarjetaCredito = nroTarjetaCredito;
	}
	public String getFranquiciaTarjetaCredito() {
		return franquiciaTarjetaCredito;
	}
	public void setFranquiciaTarjetaCredito(String franquiciaTarjetaCredito) {
		this.franquiciaTarjetaCredito = franquiciaTarjetaCredito;
	}
	public String getMesVencimiento() {
		return mesVencimiento;
	}
	public void setMesVencimiento(String mesVencimiento) {
		this.mesVencimiento = mesVencimiento;
	}
	public String getAnioVencimiento() {
		return anioVencimiento;
	}
	public void setAnioVencimiento(String anioVencimiento) {
		this.anioVencimiento = anioVencimiento;
	}
	public String getCvc() {
		return cvc;
	}
	public void setCvc(String cvc) {
		this.cvc = cvc;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public static class PasajeroBuilder {

		private String nombre;
		private String apellidoPaterno;
		private String apellidoMaterno;
		private String tipoDocumento;
		private String nroDocumento;
		private String nroTarjetaCredito;
		private String franquiciaTarjetaCredito;
		private String mesVencimiento;
		private String anioVencimiento;
		private String cvc;
		private String email;
		private String id;

        public PasajeroBuilder(){

        }        

        public PasajeroBuilder conNombre(String nombre){
            this.nombre = nombre;
            return this;
        }

        public PasajeroBuilder conApellidoPaterno(String apellidoPaterno){
            this.apellidoPaterno = apellidoPaterno;
            return this;
        }

        public PasajeroBuilder conApellidoMaterno(String apellidoMaterno){
            this.apellidoMaterno = apellidoMaterno;
            return this;
        }

        public PasajeroBuilder tipoDocumento(String tipoDocumento){
            this.tipoDocumento = tipoDocumento;
            return this;
        }

        public PasajeroBuilder conNroDocumento(String nroDocumento){
            this.nroDocumento = nroDocumento;
            return this;
        }
        
        public PasajeroBuilder conNroTarjetaCredito(String nroTarjetaCredito){
            this.nroTarjetaCredito = nroTarjetaCredito;
            return this;
        }
        
        public PasajeroBuilder conFranquiciaTarjetaCredito(String franquiciaTarjetaCredito){
            this.franquiciaTarjetaCredito = franquiciaTarjetaCredito;
            return this;
        }
        
        public PasajeroBuilder conMesVencimiento(String mesVencimiento){
            this.mesVencimiento = mesVencimiento;
            return this;
        }
        
        public PasajeroBuilder conAnioVencimiento(String anioVencimiento){
            this.anioVencimiento = anioVencimiento;
            return this;
        }
        
        public PasajeroBuilder conCvc(String cvc){
            this.cvc = cvc;
            return this;
        }
        
        public PasajeroBuilder conEmail(String email){
            this.email = email;
            return this;
        }
        
        public PasajeroBuilder conId(String id){
            this.id = id;
            return this;
        }
        
        
        


        public Pasajero build() throws NoSuchAlgorithmException {
        	Pasajero pasajero = new Pasajero();
            pasajero.nombre = this.nombre;
            pasajero.apellidoPaterno = this.apellidoPaterno;
            pasajero.apellidoMaterno = this.apellidoMaterno;
            pasajero.tipoDocumento = this.tipoDocumento;
            pasajero.nroDocumento = this.nroDocumento;
            pasajero.nroTarjetaCredito = this.nroTarjetaCredito;
            pasajero.franquiciaTarjetaCredito = this.franquiciaTarjetaCredito;
            pasajero.mesVencimiento = this.mesVencimiento;
            pasajero.anioVencimiento = this.anioVencimiento;
            pasajero.cvc = this.cvc;
            pasajero.email = this.email;
            pasajero.id = this.id;
            

            return pasajero;
        }
    }
	
}

