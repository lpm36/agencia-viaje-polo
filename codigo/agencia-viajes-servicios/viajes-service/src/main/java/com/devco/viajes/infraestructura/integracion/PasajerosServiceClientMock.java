package com.devco.viajes.infraestructura.integracion;

import java.util.ArrayList;
import java.util.List;

import com.devco.viajes.dominio.entidades.Pasajero;

public class PasajerosServiceClientMock implements PasajerosServiceClient{
	private List<Pasajero> pasajeros;
	
	public PasajerosServiceClientMock(){
		pasajeros = new ArrayList<>();
	}

	@Override
	public String registrarPasajero(Pasajero pasajero) {
		pasajeros.add(pasajero);
		return pasajero.getId();
	}
}
