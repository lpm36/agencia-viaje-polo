package com.devco.viajes.dominio.servicios;

import org.springframework.stereotype.Service;

import com.devco.viajes.dominio.entidades.ReservaViaje;

@Service
public class ReservaServiceImpl implements ReservaService{

	@Override
	public ReservaViaje guardarReserva(ReservaViaje reserva) {
		if(reserva.getVueloIda()==null ){
			return null;
		}
		if(reserva.getVueloRegreso()==null ){
			return null;
		}
		
		
		return reserva;
	}
	
}
