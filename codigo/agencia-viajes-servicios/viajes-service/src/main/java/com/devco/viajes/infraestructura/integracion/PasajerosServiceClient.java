package com.devco.viajes.infraestructura.integracion;

import org.springframework.stereotype.Service;

import com.devco.viajes.dominio.entidades.Pasajero;


public interface PasajerosServiceClient {

	public String registrarPasajero(Pasajero pasajero);
}
