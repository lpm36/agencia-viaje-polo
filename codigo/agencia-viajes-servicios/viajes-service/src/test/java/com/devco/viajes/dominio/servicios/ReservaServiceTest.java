package com.devco.viajes.dominio.servicios;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import java.security.NoSuchAlgorithmException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import com.devco.viajes.dominio.entidades.ReservaViaje;
import com.devco.viajes.dominio.entidades.Vuelo;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ReservaServiceTest {

	@Autowired
	ReservaService reservaService;
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
	
	
	@Test
	public void retornarErrorCuandoVueloDeIdaNoEstaSeteado() throws NoSuchAlgorithmException {
		ReservaViaje reserva = new ReservaViaje.ReservaViajeBuilder().conVueloVuelta(new Vuelo()).build();
		
		ReservaViaje reservaBack = reservaService.guardarReserva(reserva);
		
		assertThat(reservaBack, nullValue());
		
	}
	
	@Test
	public void retornarErrorCuandoVueloDeVueltaNoEstaSeteado() throws NoSuchAlgorithmException {
		ReservaViaje reserva = new ReservaViaje.ReservaViajeBuilder().conVueloIda(new Vuelo()).build();
		
		ReservaViaje reservaBack = reservaService.guardarReserva(reserva);
		
		assertThat(reservaBack, nullValue());
	}
	
	@Test
	public void retornarBienCuandoVuelosEstanSeteados() throws NoSuchAlgorithmException {
		ReservaViaje reserva = new ReservaViaje.ReservaViajeBuilder().conVueloIda(new Vuelo()).conVueloVuelta(new Vuelo()).build();
		
		ReservaViaje reservaBack = reservaService.guardarReserva(reserva);
		
		assertThat(reservaBack.getIdReserva(), equalTo(reserva.getIdReserva()));
	}
}
