package com.devco.viajes.dominio.servicios;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

import java.util.Collections;
import java.util.List;

//import org.hamcrest.core.IsNot;
//import org.hamcrest.core.IsNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.devco.viajes.dominio.entidades.Pasajero;
import com.devco.viajes.infraestructura.integracion.PasajerosServiceClient;
//import com.devco.viajes.dominio.entidades.ReservaViaje;
//import com.devco.viajes.dominio.entidades.Vuelo;
import com.devco.viajes.infraestructura.integracion.PasajerosServiceClientMock;
//import com.devco.viajes.infraestructura.integracion.VuelosDisponiblesAviancaClientMock;

//@ActiveProfiles("test")
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class PasajeroServiceImpTest {

	@Autowired
	PasajerosServiceClientMock pasajeroServiceClientMock;
		
	
	
	@Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }
	
	@Test
	public void registrarPasajeroNoRegistroFaltaNombreEstaNoOK() {
        Pasajero reservaViaje1 = Mockito.mock(Pasajero.class);
        Mockito.when(pasajeroServiceClientMock.registrarPasajero(reservaViaje1)).thenReturn("1");

        String vuelosDisponibles = pasajeroServiceClientMock.registrarPasajero(reservaViaje1);
        assertThat(vuelosDisponibles, is(vuelosDisponibles));

        //vuelosDisponibles.stream().forEach( x -> System.out.println(x.getIdVuelo()));
       // assertThat(vuelosDisponibles, );
    }
}
